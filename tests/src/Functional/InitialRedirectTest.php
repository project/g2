<?php

declare(strict_types=1);

namespace Drupal\Tests\g2\Functional;

use Drupal\Core\Url;
use Drupal\g2\G2;
use Drupal\Tests\BrowserTestBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests the redirect method in the Initial class.
 *
 * @group g2
 */
class InitialRedirectTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    G2::NAME,
    'statistics',
  ];

  /**
   * Provides test cases for the redirect method.
   *
   * @return array<string,array{int,int,?int,int}>
   *   The test cases.
   */
  public static function redirectTestCases(): array {
    return [
      'max age 0' => [0, 302, NULL, 86400],
      'max age 3600' => [3600, 302, 86400, 86400],
    ];
  }

  /**
   * Tests the redirect method with different configurations.
   *
   * @dataProvider redirectTestCases
   */
  public function testRedirectMethod(int $configMaxAge, int $expectedStatus, ?int $expectedMaxAge, int $expectedDCMA): void {
    $this->config('system.performance')
      ->set('cache.page.max_age', $configMaxAge)
      ->save();

    // We do not want the complete URL because we do not want the base_url path,
    // if any exists, as it would get duplicated in the actual HTTP request,
    // hence fail on non-root hosted sites, like dispatcher.drupalci.org, which
    // runs test in a /subdirectory path.
    $path = Url::fromRoute(G2::ROUTE_INITIAL_BARE)->getInternalPath();

    // Cf. BigPipeTest / BrowserTestBaseTest.
    /** @var \Behat\Mink\Driver\BrowserKitDriver $driver */
    $driver = $this->getSession()->getDriver();
    $driver->getClient()->followRedirects(FALSE);
    $this->maximumMetaRefreshCount = 0;
    $this->drupalGet("/$path");

    // Get the response object.
    $sess = $this->getSession();

    $actualStatus = $sess->getStatusCode();
    $this->assertEquals($expectedStatus, $actualStatus);

    $actualLocation = $sess->getResponseHeader('Location');
    $expectedLocation = Url::fromRoute(G2::ROUTE_MAIN, [], ['absolute' => TRUE])
      ->toString(TRUE)
      ->getGeneratedUrl();
    $this->assertEquals($expectedLocation, $actualLocation);

    $actualDCMA = $sess->getResponseHeader('X-Drupal-Cache-Max-Age');
    $this->assertEquals($expectedDCMA, $actualDCMA);

    $cc = $sess->getResponseHeader('Cache-Control') ?: '';
    if (is_null($expectedMaxAge)) {
      $this->assertDoesNotMatchRegularExpression("/max-age/", $cc);
    }
    else {
      $res = new Response();
      $res->headers->set('Cache-Control', $cc);
      $actualCCMA = $res->getMaxAge();
      $this->assertEquals($expectedMaxAge, $actualCCMA);
    }
  }

}
