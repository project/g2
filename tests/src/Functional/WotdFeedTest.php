<?php

declare(strict_types=1);

namespace Drupal\Tests\g2\Functional;

use Drupal\Core\Url;
use Drupal\g2\G2;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the WOTD feed as a user agent.
 *
 * @group G2
 */
class WotdFeedTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'statistics',
    G2::NAME,
  ];

  /**
   * Test for issue #3371672: WOTD feed must work even without entries.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFeed3371672(): void {
    $url = Url::fromRoute(G2::ROUTE_FEED_WOTD)->getInternalPath();
    $this->drupalGet($url);
    $wa = $this->assertSession();
    $wa->statusCodeEquals(200);
    $wa->responseHeaderEquals("Content-Type", "application/rss+xml; charset=utf-8");
  }

}
