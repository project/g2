<?php

declare(strict_types=1);

namespace Drupal\Tests\g2\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\g2\G2;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Route access tests.
 *
 * @group g2
 */
class AccessTest extends BrowserTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // The test builds links to controller routes within Views UI.
    'statistics',
    'views_ui',
    G2::NAME,
  ];

  /**
   * Data provider for testAdminSettingsAccess.
   *
   * @return array<string,string[]>
   *   The routes to test access to.
   */
  public static function providerAdminSettingsAccess(): array {
    $routes = [
      G2::ROUTE_CONFIG_API,
      G2::ROUTE_CONFIG_CONTROLLERS,
      G2::ROUTE_CONFIG_FORMATTING,
      G2::ROUTE_CONFIG_SERVICES,
    ];
    $tests = [];
    foreach ($routes as $route) {
      $tests[$route] = [$route];
    }
    return $tests;
  }

  /**
   * Make sure only appropriate users have access to the settings page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   *
   * @dataProvider providerAdminSettingsAccess
   */
  public function testAdminSettingsAccess(string $name): void {
    /** @var \Drupal\Core\Routing\UrlGeneratorInterface $ug */
    $ug = $this->container->get('url_generator');

    $settingsPath = $ug->generateFromRoute($name, [], ['absolute' => TRUE]);
    $this->assertIsString($settingsPath);
    $account = $this->drupalCreateUser([G2::PERM_ACCESS]);
    $this->assertInstanceOf(UserInterface::class, $account);
    $this->drupalLogin($account);
    $this->drupalGet($settingsPath);

    // Access to settings denied to normal users.
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    // Access to settings granted to site admins.
    $account = $this->drupalCreateUser(['administer site configuration']);
    $this->assertInstanceOf(UserInterface::class, $account);
    $this->drupalLogin($account);
    $this->drupalGet($settingsPath);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
  }

}
