<?php

declare(strict_types=1);

namespace Drupal\Tests\g2\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\filter\Entity\FilterFormat;
use Drupal\g2\G2;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Class G2IssuesTest contains G2 non-regression tests.
 *
 * Test once-fixed issues to catch regressions.
 *
 * @group G2
 */
class CreateTest extends BrowserTestBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin account, used for privileged operations.
   *
   * @var \Drupal\user\UserInterface
   */
  public UserInterface $admin;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'statistics',
    G2::NAME,
  ];

  /**
   * {@inheritdoc}
   *
   * Note: "administer g2 entries" does not subsume "create g2_entry content".
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();
    $type = G2::BUNDLE;
    $admin = $this->drupalCreateUser([
      G2::PERM_ACCESS,
      "create {$type} content",
    ]);
    $this->assertInstanceOf(UserInterface::class, $admin);
    $this->admin = $admin;
  }

  /**
   * HTML test being generated in the "title" attribute on node auto-creation.
   */
  public function test1243170version10(): void {
    $this->config(G2::CONFIG_NAME)
      ->set(G2::VARREMOTEG2, '')
      ->set(G2::VARTOOLTIPS, TRUE)
      ->save();

    $formatID = 'test_format';
    FilterFormat::create([
      'format' => $formatID,
      'name' => 'A test format supporting G2 definition',
      'status' => 1,
      'filters' => [
        'g2_definition' => [
          'status' => 1,
          'settings' => [],
        ],
      ],
    ])->save();

    $settings = [
      'type' => G2::BUNDLE,
      'title' => 'CSS',
      'body' => [
        'value' => 'A style language for <dfn>HTML</dfn>',
        'format' => $formatID,
      ],
    ];
    $node = $this->drupalCreateNode($settings);

    // Step 1: ensure the DFN filter generates the proper "a.href".
    $this->drupalLogin($this->admin);
    $this->drupalGet(Url::fromRoute(G2::ROUTE_NODE_CANONICAL,
      ['node' => $node->id()],
      ['absolute' => TRUE],
    ));
    $this->assertSession()
      ->linkExists('HTML', 0,
        'Found G2 "HTML" link on node page',
      );

    // Step 2: visit homonyms page, for the lack of entry, and the create link.
    $link = $this->xpath("//a[@class='g2-dfn-link']");
    if (empty($link)) {
      return;
    }
    $link0 = $link[0] ?? NULL;
    $this->assertInstanceOf(NodeElement::class, $link0);
    $href = $link0->getAttribute('href');
    $this->assertNotEmpty($href);
    $this->drupalGet($href);
    $this->assertSession()
      ->pageTextContains("There are currently no entries for");

    // Step 3: verify the create link target.
    $this->assertSession()
      ->linkExists('create one', 0,
        'Found G2 "create" link on g2/homonyms/HTML',
      );
    $link = $this->xpath('//a[text()="create one"]');
    if (empty($link)) {
      return;
    }
    $link0 = $link[0] ?? NULL;
    $this->assertInstanceOf(NodeElement::class, $link0);
    $href = $link0->getAttribute('href');
    $this->assertIsString($href);
    // Do NOT compare the plain href to the path: it misses the base URL.
    // Do NOT compare by generating the URL with Url::fromRoute, because that is
    // already what Homonyms does, so it could not fail.
    $base = base_path();
    $path = parse_url($href, PHP_URL_PATH);
    $this->assertEquals("{$base}node/add/g2_entry", $path,
      'Found G2 node creation link on g2/homonyms/HTML');
    $query = parse_url($href, PHP_URL_QUERY);
    $this->assertEquals('title=HTML', $query,
      'G2 node creation link contains title');
  }

}
