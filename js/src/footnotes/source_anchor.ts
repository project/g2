import { Definition } from "./definition";

/**
 * SourceAnchor represents a definition link in its DOM context.
 */
class SourceAnchor {
  /**
   * The prefix to form IDs for footnotes in the footnotes block.
   */
  public static readonly fnPrefix = "g2-fn-";

  constructor(
    public index: number, // The sequence index of the element in the list of anchors for the DOM.
    public definition: Definition, // The link definition.
    public elem: HTMLAnchorElement, // The anchor element as found in the DOM
  ) {}

  public renderAnchor(di: number): void {
    const doc = this.elem.ownerDocument;
    const $e = this.elem;
    $e.removeAttribute("href");
    $e.id = `g2-dfn-${this.index}`;
    const $sup = doc.createElement("SUP");
    const $fa = doc.createElement("A") as HTMLAnchorElement;
    $fa.href = `#${SourceAnchor.fnPrefix}${di}`;
    $fa.text = String(this.index);
    $sup.appendChild($fa);
    $e.append($sup);
  }
}

export { SourceAnchor };
