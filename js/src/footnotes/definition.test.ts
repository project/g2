import { describe, expect, test } from "@jest/globals";
import { Definition } from "./definition";

describe("Basic class operations", () => {
  test.each([
    ["fully set", "t", "u", "i", `{"text":"t","url":"u","title":"i"}`],
    ["no text", "", "url", "title", `{"url":"url","title":"title"}`],
    ["no url", "text", "", "title", `{"text":"text","title":"title"}`],
    ["no title", "text", "url", "", `{"text":"text","url":"url"}`],
    ["nothing set", "", "", "", "{}"],
  ])(
    "%s",
    (
      _title: string,
      text: string,
      url: string,
      title: string,
      expected: string,
    ) => {
      const def = new Definition(text, url, title);
      const s: string = def.toString();
      expect(s).toBe(expected);
    },
  );
});
