import { Definition } from "./definition";
import { SourceAnchor } from "./source_anchor";

/**
 * Footnote represents a definition footnote in the footnotes block.
 */
class Footnote extends SourceAnchor {
  public constructor(
    doc: Document,
    index: number,
    def: Definition,
    public sourceAnchors: SourceAnchor[] = [], // The references to/from that footnote on the page.
  ) {
    const elem: HTMLAnchorElement = doc.createElement("a");
    elem.text = def.text;
    elem.title = def.title;
    elem.href = def.url;
    super(index, def, elem);
    this.sourceAnchors = sourceAnchors;
  }

  /**
   * Render a footnote with the expected shape.
   *
   * @param ol
   *   The ordered list in which to render it.
   */
  public renderNote(ol: HTMLOListElement): void {
    /* Shape is inspired by Wikipedia footnotes:
<li id="g2-fn-(di)">
  <span class="g2-backlinks">
    ↑
    <sup>
      <a href="#g2-dfn-(si)">(si)</a>, <a href="#g2-dfn-(si)">(si)</a> ...
    </sup>
  </span>
  <span class="g2-reference-text">
    <a href="(def.url)">(def.title)</a>
  </span>
</li>
*/
    const doc = ol.ownerDocument;
    const li = doc.createElement("LI") as HTMLLIElement;
    li.id = `${SourceAnchor.fnPrefix}${this.index}`;

    const backlinks = doc.createElement("SPAN") as HTMLSpanElement;
    backlinks.classList.add("g2-backlinks");
    const up: Text = doc.createTextNode("↑ ");
    const sp: Text = doc.createTextNode(" ");
    backlinks.appendChild(up);
    const sup = doc.createElement("SUP") as HTMLElement;
    const comma: Text = doc.createTextNode(", ");
    for (let i = 0; i < this.sourceAnchors.length; i++) {
      const sa: SourceAnchor = this.sourceAnchors[i];
      const link = doc.createElement("A") as HTMLAnchorElement;
      link.href = `#g2-dfn-${sa.index}`;
      link.text = String(sa.index);
      sup.appendChild(link);
      if (i < this.sourceAnchors.length - 1) {
        sup.append(comma);
      }
    }
    backlinks.appendChild(sup);
    backlinks.append(sp);
    li.appendChild(backlinks);

    const ref = doc.createElement("SPAN") as HTMLSpanElement;
    ref.classList.add("g2-reference-text");
    const da = doc.createElement("A") as HTMLAnchorElement;
    da.href = this.definition.url;
    da.text = this.definition.title;
    ref.appendChild(da);
    li.appendChild(ref);
    ol.appendChild(li);
  }
}
export { Footnote };
