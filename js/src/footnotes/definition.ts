/**
 * Definition represents the DOM-unaware contents of a definition link.
 */
class Definition {
  public constructor(
    public text: string, // The text around which to wrap the link.
    public url: string, // The target URL.
    public title: string, // The title to place on the URL anchor.
  ) {}

  /**
   * Is the definition completely not empty?
   */
  public isFullySet(): boolean {
    return !!this.url && !!this.title && !!this.text;
  }

  public toString(): string {
    return JSON.stringify(this, (k: string, v) => v || undefined);
  }
}

export { Definition };
