import { G2footnotes } from "./g2footnotes";

declare global {
  let Drupal: any;
  let once: any;
}

(() => {
  // We create this instance here so it can be reused across attach calls.
  // TODO: support incremental rebuilding for multiple attachments.
  // This is to support new links appearing over time. e.g. due to BigPipe.
  const g = new G2footnotes();

  Drupal.behaviors.g2 = {
    attach(context) {
      if (
        // Without BigPipe: transform on document attach.
        context instanceof Document ||
        // With BigPipe: transform on the G2 footnotes block attach.
        (context.tagName === "DIV" &&
          /^block-g2footnotes-\d+$/.test(context.id))
      ) {
        once("g2", "body").forEach(() => {
          g.transform();
        });
      }
    }
  };
})();
