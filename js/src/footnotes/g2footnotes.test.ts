/**
 * @jest-environment jsdom
 */

import { afterEach, beforeEach, describe, expect, test } from "@jest/globals";
import { Footnote, G2footnotes, SourceAnchor } from "./g2footnotes";

describe("Footnotes operation outside Drupal", () => {
  beforeEach(() => {
    document.body.innerHTML = `
The <a href="url-foo" title="title-foo" class="g2-dfn-link">foo</a>
may be a <a href="url-bar" title="title-bar" class="g2-dfn-link">bar</a>,
but is remains a <a href="url-foo" title="title-foo" class="g2-dfn-link">foo</a>.

And yet, <a href="url-foo" class="g2-dfn-link">foo</a> is not a definition,
as it misses a title attribute.

<div class="g2-footnotes"></div>
    `;
  });

  afterEach(() => {
    document.body.innerHTML = "";
  });

  test("get all source links", () => {
    const k = new G2footnotes(document);
    const sas = k.anchorsFromSource();

    // The last one has no title, so we skip it.
    expect(sas.length).toBe(3);

    let index = -1;
    for (const sa of sas) {
      expect(sa.index).toBeGreaterThan(index);
      index = sa.index;
      expect(sa.definition.isFullySet()).toBeTruthy();
    }
  });

  test("build footnotes list", () => {
    const k = new G2footnotes(document);
    const sas = k.anchorsFromSource();
    // Nothing to test here: this is tested in "get all source links".
    const fns = k.footnotesFromAnchors(sas);
    // 2 sources are identical, so there are only 2 footnotes.
    expect(fns.length).toBe(2);
    expect(fns[0].sourceAnchors.length).toBe(2);
    expect(fns[1].sourceAnchors.length).toBe(1);
  });

  test("build backlinks in footnotes", () => {
    const k: G2footnotes = new G2footnotes(document);
    const fns: Footnote[] = k.footnotesFromAnchors(k.anchorsFromSource());
    k.buildFootnotes(fns);
    const $olList = document.querySelectorAll(".g2-footnotes ol");
    expect($olList.length).toBe(1);
    const $ol = $olList[0];

    expect($ol.childElementCount).toBe(2); // 2 footnotes
    const $fn0 = $ol.childNodes[0] as HTMLLIElement;
    expect($fn0).toBeInstanceOf(HTMLLIElement);
    expect($fn0.hasAttribute("id")).toBeTruthy();
    expect($fn0.id).toBe(`${SourceAnchor.fnPrefix}1`);
    // span.g2-backlinks and span.g2-reference-text
    expect($fn0.children.length).toBe(2);
    const backlinks = $fn0.children[0];
    expect(backlinks).toBeInstanceOf(HTMLSpanElement);
    expect(backlinks.classList.toString()).toEqual("g2-backlinks");
    expect(backlinks.children.length).toBe(1); // sup
    expect(backlinks.childNodes.length).toBe(3); // up, sup, and sp.
    const sup = backlinks.children[0];
    expect(sup.children.length).toBe(2); // 2 links
    expect(sup.childNodes.length).toBe(3); // 2 links, comma

    const $fn1 = $ol.childNodes[1] as HTMLLIElement;
    expect($fn1).toBeInstanceOf(HTMLLIElement);
    expect($fn1.hasAttribute("id")).toBeTruthy();
    expect($fn1.id).toBe(`${SourceAnchor.fnPrefix}2`);
  });

  test("convert anchors", () => {
    const k: G2footnotes = new G2footnotes(document);
    const sas = k.anchorsFromSource();
    const fns = k.footnotesFromAnchors(sas);
    // Nothing to test here: this is tested in "get all source links".
    k.convertAnchors(fns);
    for (let i = 0; i < sas.length; i++) {
      const $e = sas[i].elem;
      expect($e.hasAttribute("href")).toBeFalsy();
      expect($e.hasAttribute("title")).toBeTruthy();
      expect($e.id).toBe(`g2-dfn-${i + 1}`);
      expect($e.children.length).toBe(1); // sup.
      expect($e.childNodes.length).toBe(2); // text and sup.
      expect($e.childNodes[0]).toBeInstanceOf(Text);
      expect($e.children[0]).toBeInstanceOf(HTMLElement);
      expect($e.children[0].tagName).toBe("SUP");
    }
  });
});
