import { Definition } from "./definition";
import { Footnote } from "./footnote";
import { SourceAnchor } from "./source_anchor";

class G2footnotes {
  /**
   * The footnotes block class.
   */
  protected readonly blockClass = "g2-footnotes";

  /**
   * This selector chooses the links, excepting any that could be in the block.
   */
  protected readonly linkSelector =
    ".g2-dfn-link:not(.g2-footnotes .g2-dfn-link)";

  protected doc: Document;

  /**
   * The footnotes block element.
   */
  protected $block: HTMLDivElement;

  constructor(doc?: Document) {
    this.doc = doc || document;
  }

  /**
   * Extract the G2 anchors from the source document.
   */
  public anchorsFromSource(): SourceAnchor[] {
    const sas: SourceAnchor[] = [];
    let si: number = 1;
    const anchors = this.doc.querySelectorAll(this.linkSelector);
    anchors.forEach((elem: HTMLAnchorElement) => {
      const def = new Definition(elem.textContent, elem.href, elem.title);
      // We only care for fully defined anchors.
      if (!def.isFullySet()) {
        return;
      }
      const sa = new SourceAnchor(si++, def, elem);
      sas.push(sa);
    });
    return sas;
  }

  /**
   * Generate the footnotes list from the source anchors.
   *
   * @param anchors
   */
  public footnotesFromAnchors(anchors: SourceAnchor[]): Footnote[] {
    const rfn: Record<string, Footnote> = {};
    let fi = 1;
    for (const anchor of anchors) {
      const key = anchor.definition.text;
      let fn = rfn[key];
      if (!fn) {
        fn = new Footnote(this.doc, fi, anchor.definition);
        fi++;
      }
      fn.sourceAnchors.push(anchor);
      rfn[key] = fn;
    }
    return Object.values(rfn);
  }

  public buildFootnotes(fns: Footnote[]): void {
    const $ol = this.doc.createElement("OL") as HTMLOListElement;
    Object.entries(fns).forEach(([, fn]) => {
      fn.renderNote($ol);
    });
    if ($ol.childElementCount !== 0) {
      this.$block = this.doc.querySelector(`.${this.blockClass}`);
      if (!(this.$block instanceof HTMLDivElement)) {
        console.error("No footnotes block in DOM, G2 Footnotes aborted");
      }

      this.$block.appendChild($ol);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  public convertAnchors(fns: Footnote[]): void {
    for (const fn of fns) {
      for (const sa of fn.sourceAnchors) {
        sa.renderAnchor(fn.index);
      }
    }
  }

  /**
   * Transform a complete page.
   *
   * Goes over all the source links, building a footnotes block from them,
   * then converting the source links to fragment links to the footnotes block.
   *
   * TODO handle incremental processing in case more links appear over time.
   * This can happen thanks to e.g. big_pipe.
   */
  public transform(): void {
    const sas = this.anchorsFromSource();
    const fns = this.footnotesFromAnchors(sas);
    this.buildFootnotes(fns);
    this.convertAnchors(fns);
  }
}

export { Definition, Footnote, SourceAnchor, G2footnotes };
