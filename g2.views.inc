<?php

/**
 * @file
 * Provide views data for g2.module.
 */

declare(strict_types=1);

use Drupal\g2\G2;

/**
 * Implements hook_views_data().
 *
 * @phpstan-return array<string,mixed>
 */
function g2_views_data(): array {
  $data = [];
  $data['g2_referer'] = [];
  $data['g2_referer']['table'] = [];
  $data['g2_referer']['table']['group'] = t('G2');
  $data['g2_referer']['table']['provider'] = G2::NAME;

  $data['g2_referer']['table']['join'] = [
    'node_field_data' => [
      'left_field' => 'nid',
      'field' => 'nid',
    ],
  ];

  $data['g2_referer']['table']['base'] = [
    'field' => 'nid',
    'title' => t('G2 Referrers'),
    'help' => t('Contains a list of referrers going to the given node'),
  ];

  $data['g2_referer']['nid'] = [
    'title' => t('Node'),
    'help' => t('The G2 node'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'base' => 'node_field_data',
      'base field' => 'nid',
      'id' => 'standard',
      'label' => t('G2 node'),
      'help' => t('Relationship between the node referrers and G2 node field data'),
    ],
  ];

  $data['g2_referer']['referer'] = [
    'title' => t('Referrer'),
    'help' => t('A URL containing a link to that G2 node'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['g2_referer']['incoming'] = [
    'title' => t('Incoming hits'),
    'help' => t('The number of hits coming from this referrer'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  return $data;
}
