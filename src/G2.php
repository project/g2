<?php

declare(strict_types=1);

namespace Drupal\g2;

use Drupal\Core\Cache\Cache;

/**
 * Class G2 is the container for general-use G2 data.
 */
class G2 {

  /**
   * The API format.
   */
  public const API_VERSION = 8;

  // The name of the node.type.g2_entry config entity.
  public const BUNDLE = 'g2_entry';

  /**
   * The config key for the module configuration.
   */
  public const CONFIG_NAME = 'g2.settings';

  /**
   * The config key for the statistics module configuration.
   */
  public const STATISTICS_CONFIG_NAME = 'statistics.settings';

  /**
   * The State default for the current stored random entry.
   */
  public const DEFRANDOMENTRY = '';

  public const DEFWOTDFEEDLINK = TRUE;

  /**
   * Block: n most viewed.
   */
  public const DELTA_TOP = 'top';

  /**
   * Block ID: word of the day.
   */
  public const DELTA_WOTD = 'g2_wotd';

  /**
   * The key for the titles MSM in the Indexer KV collection.
   */
  public const KV_MSM = 'g2:msm';

  /**
   * The key for the titles map in the Indexer KV collection.
   */
  public const KV_TITLES = 'g2:titles';

  /**
   * The name of the G2 admin front library.
   */
  public const LIB_ADMIN = self::NAME . '/g2.admin';

  /**
   * The name of the G2 alphabar styling library.
   */
  public const LIB_ALPHABAR = self::NAME . '/' . self::SVC_ALPHABAR;

  /**
   * The name of the G2 footnotes JS library.
   */
  const LIB_FOOTNOTES = self::NAME . '/g2.footnotes';

  /**
   * The module name.
   *
   * Meant to be used to identify the origin of stored data emanating from this
   * module in another subsystem, e.g. logs, as happens with logger.channel.g2.
   */
  public const NAME = 'g2';

  // Constants in this group are only listed to remove WSODs, but they still
  // need to be converting from hook_menu to Symfony routing.
  public const PATH_INITIAL = 'g2/initial';

  public const PATH_NODE_ADD = 'node/add/g2';

  /**
   * The most basic core permission, normally granted to anonymous users.
   */
  public const PERM_ACCESS = 'access content';

  /**
   * The G2 permission for administrators.
   */
  public const PERM_ADMIN = 'administer g2 entries';

  /**
   * Route: the core content administration route.
   */
  public const ROUTE_ADMIN_CONTENT = 'system.admin_content';

  /**
   * Route: autocomplete G2 entry by title.
   */
  public const ROUTE_AUTOCOMPLETE_ENTRY = 'g2.autocomplete.entry';

  /**
   * Route: autocomplete route by name, only listing routes without parameters.
   */
  public const ROUTE_AUTOCOMPLETE_ROUTE_0PARAM = 'g2.autocomplete.route_0param';

  /**
   * Route: autocomplete route by name, only listing routes with one parameter.
   */
  public const ROUTE_AUTOCOMPLETE_ROUTE_1PARAM = 'g2.autocomplete.route_1param';

  /**
   * Route: the G2 API configuration form.
   */
  public const ROUTE_CONFIG_API = 'g2.settings.api';

  /**
   * Route: the G2 controllers configuration form.
   */
  public const ROUTE_CONFIG_CONTROLLERS = 'g2.settings.controllers';

  /**
   * Route: the G2 formatting configuration form.
   */
  public const ROUTE_CONFIG_FORMATTING = 'g2.settings.formatting';

  /**
   * Route: the G2 services configuration form.
   */
  public const ROUTE_CONFIG_SERVICES = 'g2.settings.services';

  /**
   * Route: The core block administration page.
   */
  public const ROUTE_BLOCKS = 'block.admin_display';

  /**
   * Route: the initials path but without an initial.
   */
  public const ROUTE_INITIAL_BARE = 'g2.initial.bare';

  /**
   * Route: expose the entries with a given title.
   */
  public const ROUTE_HOMONYMS = 'g2.homonyms';

  /**
   * Route: WOTD RSS feed.
   */
  public const ROUTE_FEED_WOTD = 'view.g2_wotd.feed_1';

  /**
   * Route: G2 main page.
   */
  public const ROUTE_MAIN = 'g2.main';

  /**
   * The name of the canonical node route.
   */
  public const ROUTE_NODE_CANONICAL = 'entity.node.canonical';

  /**
   * Route: show referers to a g2_entry.
   */
  public const ROUTE_REFERERS = 'g2.node_referers';

  /**
   * Route: Field UI node display modes.
   */
  public const ROUTE_VM = 'entity.entity_view_display.node.view_mode';

  /**
   * Route: form offering to wipe all referers.
   */
  public const ROUTE_WIPE_ALL = 'g2.wipe.all';

  /**
   * Route: form offering to wipe referers for a single G2 entry.
   */
  public const ROUTE_WIPE_ONE = 'g2.wipe.one';

  /**
   * The name of the g2.alphabar service.
   */
  public const SVC_ALPHABAR = 'g2.alphabar';

  /**
   * The name of the core config.factory service.
   */
  public const SVC_CONF = 'config.factory';

  /**
   * The name of the core entity_type.manager service.
   */
  public const SVC_ETM = 'entity_type.manager';

  /**
   * The name of the G2 matcher service.
   */
  public const SVC_MATCHER = 'g2.matcher';

  /**
   * The name of the g2.latest service.
   */
  public const SVC_LATEST = 'g2.latest';

  /**
   * The logger.channel.g2 service.
   */
  public const SVC_LOGGER = 'logger.channel.g2';

  /**
   * The code plugin.manager.block service.
   */
  public const SVC_PM_BLOCK = 'plugin.manager.block';

  /**
   * The name of the g2.random service.
   */
  public const SVC_RANDOM = 'g2.random';

  /**
   * The name of the g2.route_filter service.
   */
  public const SVC_ROUTE_FILTER = 'g2.route_filter';

  /**
   * The state service.
   */
  public const SVC_STATE = 'state';

  /**
   * The name of the g2.test.logger service.
   *
   * Beware: this is NOT a Drupal logger channel, just a PSR-3 test helper.
   */
  public const SVC_TEST_LOGGER = 'g2.test.logger';

  /**
   * The name of the g2.top service.
   */
  public const SVC_TOP = 'g2.top';

  /**
   * The name of the g2.referer_tracker service.
   */
  public const SVC_TRACKER = 'g2.referer_tracker';

  /**
   * The name of the g2.wotd service.
   */
  public const SVC_WOTD = 'g2.wotd';

  /**
   * The name of the SQL table holding the G2 referer information.
   */
  public const TBL_REFERER = 'g2_referer';

  public const TITLE_MAIN = 'Glossary';

  public const TOOLTIPS_NONE = 0;

  public const TOOLTIPS_TITLES = 1;

  public const TOOLTIPS_TEASERS = 2;

  /**
   * In this version, G2 entries are a node bundle (content type).
   *
   * This is likely to change as some point in the future.
   */
  public const TYPE = 'node';

  /**
   * The config path for the XML-RPC API server enabling.
   */
  public const VARAPIENABLED = 'api.server.enabled';

  /**
   * The config path for the api throttle limiter.
   */
  public const VARAPITHROTTLE = 'api.server.throttle';

  /**
   * The config path for the route service the "entries by initial" page.
   */
  public const VARCONTROLLERINITIAL = 'controller.initial.route';

  public const VARCONTROLLERHOMONYMS = 'controller.homonyms';

  /**
   * The config path for the alphabar contents.
   */
  public const VARALPHABARCONTENTS = 'services.alphabar.contents';

  /**
   * The config path for the protection against core bug 3369926.
   *
   * @todo Remove after #3369926 is fixed.
   */
  public const VARALPHABAR3369926 = 'services.alphabar.issue3369926';

  /**
   * The config path for the alphabar visual row length.
   */
  public const VARALPHABARROWLENGTH = 'block.alphabar.row_length';

  /**
   * The config path for the homonyms redirect on single match.
   */
  public const VARHOMONYMSREDIRECTSINGLE = 'controller.homonyms.redirect_on_single_match';

  /**
   * The config path for the homonyms listing node.
   *
   * @deprecated in g2:8.x-1.0 and is removed from g2:8.x-2.0. Use a view.
   * @see https://www.drupal.org/project/g2/issues/3369887
   */
  public const VARHOMONYMSNID = 'controller.homonyms.nid';

  /**
   * The config path for the homonyms page route.
   */
  public const VARHOMONYMSROUTE = 'controller.homonyms.route';

  /**
   * The config path for the homonyms listing view.
   */
  public const VARHOMONYMSVID = 'controller.homonyms.vid';

  /**
   * The config path for the homonyms redirect HTTP status.
   */
  public const VARHOMONYMSREDIRECTSTATUS = 'controller.homonyms.redirect_status';

  /**
   * The config path for the Latest(n) block entry count.
   */
  public const VARLATESTCOUNT = 'block.latest.count';

  /**
   * The config path for the Latest(n) service maximum entry count.
   */
  public const VARLATESTMAXCOUNT = 'services.latest.max_count';

  /**
   * The config path for the referrer logging feature.
   */
  public const VARLOGREFERERS = 'controller.referers.track';

  /**
   * The config path for the node used to build the main G2 page.
   *
   * @deprecated in g2:8.x-1.0 and is removed from g2:8.x-2.0. Use a view.
   * @see https://www.drupal.org/project/g2/issues/3369887
   */
  public const VARMAINNID = 'controller.main.nid';

  /**
   * The config path for the main page route.
   *
   * Always use it to build URLs instead of assuming route g2.main.
   */
  public const VARMAINROUTE = 'controller.main.route';

  /**
   * The config path for the free tagging choice on node formatting.
   */
  public const VARNOFREETAGGING = 'formatting.hide_free_tagging';

  /**
   * The config path for the title override on G2 entry pages.
   */
  public const VARPAGETITLE = 'formatting.title';

  /**
   * The State key for the current stored random entry.
   */
  public const VARRANDOMENTRY = 'g2.random-entry';

  /**
   * The config path for the random entry storage choice.
   */
  public const VARRANDOMSTORE = 'services.random.store';

  /**
   * The config path for the URL of the remote glossary server.
   */
  public const VARREMOTEG2 = 'api.client.remote';

  /**
   * The config path for the level of tooltips features.
   */
  public const VARTOOLTIPS = 'formatting.tooltips_level';

  /**
   * The config path for the Top(n) block entry count.
   */
  public const VARTOPCOUNT = 'block.top.count';

  /**
   * The config path for the Top(n) service maximum entry count.
   */
  public const VARTOPMAXCOUNT = 'services.top.max_count';

  /**
   * The config path for the WOTD auto_change property.
   */
  public const VARWOTDAUTOCHANGE = 'services.wotd.auto_change';

  /**
   * The state path for the WOTD auto_change date.
   *
   * The associated value is a date("Y-m-d").
   */
  public const VARWOTDDATE = 'g2.wotd-date';

  /**
   * The config path for the WOTD entry.
   */
  public const VARWOTDENTRY = 'services.wotd.entry';

  public const VARWOTDFEEDAUTHOR = 'g2-wotd-feed-author';

  public const VARWOTDFEEDLINK = 'g2-wotd-feed-link';

  /**
   * The public-facing version: two first levels for semantic versioning.
   */
  public const VERSION = '8.1';

  /**
   * The ID of the Authored view.
   */
  public const VIEW_AUTHORED = 'g2_authored';

  /**
   * The ID of the block display in the Authored view.
   */
  public const VIEW_AUTHORED_DISPLAY = 'user_page_block';

  /**
   * The ID of the WOTD view.
   */
  public const VIEW_WOTD = 'g2_wotd';

  /**
   * The ID of the feed display in the WOTD view.
   */
  public const VIEW_WOTD_DISPLAY = 'feed_1';

  // View modes.
  public const VM_BLOCK = 'g2_block';

  /**
   * The view display used on the homonyms "plain node list" page.
   */
  public const VM_ENTRY_LIST = 'g2_entry_list';

  /**
   * The view display used on the deprecated homonyms node-based page.
   *
   * @deprecated in g2:8.x-1.0 and is removed from g2:8.x-2.0. Use a view.
   * @see https://www.drupal.org/project/g2/issues/3369887
   */
  public const VM_HOMONYMS_PAGE = 'g2_homonyms_page';

  /**
   * The core RSS view display.
   */
  public const VM_RSS = 'rss';

  /**
   * The view display used for tooltips on definition links.
   */
  public const VM_TOOLTIPS = 'g2_tooltips';

  /**
   * Return the API version.
   *
   * @return int
   *   The version of the API format.
   */
  public static function api(): int {
    return static::API_VERSION;
  }

  /**
   * Encodes terminal path portions for G2.
   *
   * This allows linking to things containing #, + or '.', like 'C++', 'C#' or
   * the '.' initial.
   *
   * Warning: this is NOT a generic replacement for urlencode, but covers a very
   * specific G2-related need.
   *
   * @param string $terminal
   *   The terminal rune to encode.
   *
   * @return string
   *   The encoded terminal.
   */
  public static function encodeTerminal(string $terminal): string {
    $terminal = strtr($terminal, [
      '.' => '%2E',
      '/' => '%2F',
      '#' => '%23',
      '&' => '%26',
      '+' => '%2B',
    ]);
    return $terminal;
  }

  /**
   * Manually invalidate the WOTD view.
   *
   * This is needed because of a core bug causing cache metadata from default
   * argument plugins not to be present in Views results.
   *
   * To work around the missing automatic invalidation, we invalidate the
   * rendered elements including the WOTD view manually.
   *
   * @see https://www.drupal.org/project/drupal/issues/3371236
   */
  public static function invalidateWotdView(): void {
    Cache::invalidateTags(["config:views.view.g2_wotd"]);
  }

}
