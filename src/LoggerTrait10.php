<?php

namespace Drupal\g2;

use Psr\Log\LoggerTrait;

/**
 * A LoggerTrait compatible with psr/log:^2|^3 as required by Drupal 10/11.
 */
trait LoggerTrait10 {
  use LoggerTrait;

  /**
   * The individual log entries, in insertion order.
   *
   * @var array{int,string|\Stringable,array<string,mixed>}[]
   */
  public array $entries = [];

  /**
   * {@inheritdoc}
   */
  public function log($level, mixed $message, array $context = []): void {
    assert(is_numeric($level));
    $level = (int) $level;
    $this->entries[] = [$level, $message, $context];
  }

}
