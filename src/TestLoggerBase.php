<?php

declare(strict_types=1);

namespace Drupal\g2;

use Psr\Log\LoggerInterface;

if (version_compare(\Drupal::VERSION, '10.0.0', '>=')) {
  /**
   * TestLoggerBase is a test helper that logs to a public array.
   */
  class TestLoggerBase implements LoggerInterface {

    use LoggerTrait10;

  }
}
else {
  /**
   * TestLoggerBase is a test helper that logs to a public array.
   */
  class TestLoggerBase implements LoggerInterface {

    use LoggerTrait9;

  }
}
