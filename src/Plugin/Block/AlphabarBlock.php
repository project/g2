<?php

declare(strict_types=1);

namespace Drupal\g2\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\g2\Alphabar;
use Drupal\g2\G2;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AlphabarBlock is the Alphabar block plugin.
 *
 * @Block(
 *   id = "g2_alphabar",
 *   admin_label = @Translation("G2 Alphabar"),
 *   category = @Translation("G2"),
 *   help = @Translation("This block displays a clickable list of initials from the G2 glossary."),
 * )
 *
 * @phpstan-consistent-constructor
 */
class AlphabarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Alphabar service.
   *
   * @var \Drupal\g2\Alphabar
   */
  protected $alphabar;

  /**
   * The g2.settings/block.alphabar configuration.
   *
   * @var array<string,mixed>
   */
  protected $blockConfig;

  /**
   * Static factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   *
   * @return static
   *   The plugin instance.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    /** @var \Drupal\g2\Alphabar $alphabar */
    $alphabar = $container->get(G2::SVC_ALPHABAR);

    /** @var \Drupal\Core\Config\ConfigFactory $configFactory */
    $configFactory = $container->get(G2::SVC_CONF);

    /** @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = $configFactory->get(G2::CONFIG_NAME);

    $block_config = $config->get('block.alphabar');

    return new static($configuration, $plugin_id, $plugin_definition,
      $alphabar, $block_config);
  }

  /**
   * Constructor.
   *
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   * @param \Drupal\g2\Alphabar $alphabar
   *   The Alphabar service.
   * @param array<string,mixed> $block_config
   *   The block configuration.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Alphabar $alphabar,
    array $block_config,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->alphabar = $alphabar;
    $this->blockConfig = $block_config;
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array<string,mixed>
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build(): array {
    $links = $this->alphabar->getLinks();
    $result = [
      '#theme' => 'g2_alphabar',
      '#alphabar' => $links,
      '#row_length' => $this->blockConfig['row_length'],
      '#attached' => [
        'library' => [G2::LIB_ALPHABAR],
      ],
      '#cache' => [
        'tags' => ["config:" . (G2::CONFIG_NAME)],
      ],
    ];

    return $result;
  }

}
