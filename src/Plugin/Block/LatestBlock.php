<?php

declare(strict_types=1);

namespace Drupal\g2\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\g2\G2;
use Drupal\g2\Latest;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LatestBlock is the Latest(n) block plugin.
 *
 * @Block(
 *   id = "g2_latest",
 *   admin_label = @Translation("G2 Latest(n)"),
 *   category = @Translation("G2"),
 *   help = @Translation("This block displays a list of the most recently
 *   updated entries in the G2 glossary."),
 * )
 *
 * @phpstan-consistent-constructor
 */
class LatestBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $etm;

  /**
   * The g2.latest service.
   *
   * @var \Drupal\g2\Latest
   */
  protected $latest;

  /**
   * Static factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   *
   * @return static
   *   The plugin instance.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
    $etm = $container->get(G2::SVC_ETM);

    /** @var \Drupal\g2\Latest $latest */
    $latest = $container->get(G2::SVC_LATEST);

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = $container->get(G2::SVC_CONF);
    return new static($configuration, $plugin_id, $plugin_definition, $configFactory, $etm, $latest);
  }

  /**
   * Constructor.
   *
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The core config.factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   The core entity_type.manager service.
   * @param \Drupal\g2\Latest $latest
   *   The g2.latest service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $etm,
    Latest $latest,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->etm = $etm;
    $this->latest = $latest;
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array<string,mixed>
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build(): array {
    $mixedCount = $this->configFactory
      ->get(G2::CONFIG_NAME)
      ->get(G2::VARLATESTCOUNT);
    assert(is_scalar($mixedCount));
    $links = $this->latest->getLinks((int) $mixedCount);
    $md = (new CacheableMetadata())
      ->addCacheTags([
        "config:" . (G2::CONFIG_NAME),
        "node_list:" . (G2::BUNDLE),
      ]);
    /** @var \Drupal\Core\Link $link */
    foreach ($links as $link) {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $link->getUrl()->getOption('entity');
      $md->addCacheableDependency($node);
    }
    $build = [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
    $md->applyTo($build);
    return $build;
  }

}
