<?php

declare(strict_types=1);

namespace Drupal\g2\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\g2\G2;
use Drupal\g2\Top;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TopBlock is the Top(n) block plugin.
 *
 * @Block(
 *   id = "g2_top",
 *   admin_label = @Translation("G2 Top(n)"),
 *   category = @Translation("G2"),
 *   help = @Translation("This block displays a list of the most viewed entries in the G2 glossary."),
 * )
 *
 * @phpstan-consistent-constructor
 */
class TopBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The g2.settings/block.top configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The g2.top service.
   *
   * @var \Drupal\g2\Top
   */
  protected $top;

  /**
   * Static factory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   *
   * @return static
   *   The plugin instance.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    /** @var \Drupal\g2\Top $top */
    $top = $container->get(G2::SVC_TOP);

    /** @var \Drupal\Core\Config\ConfigFactory $configFactory */
    $configFactory = $container->get(G2::SVC_CONF);

    return new static($configuration, $plugin_id, $plugin_definition, $configFactory, $top);
  }

  /**
   * Constructor.
   *
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array<string,mixed> $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The core config.factory service.
   * @param \Drupal\g2\Top $top
   *   The g2.top service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    ConfigFactoryInterface $configFactory,
    Top $top,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->top = $top;
    $this->configFactory = $configFactory;
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array<string,mixed>
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build(): array {
    if (!($this->top->isAvailable())) {
      return [];
    }
    $config = $this->configFactory->get(G2::CONFIG_NAME);
    $mixedBlockCount = $config->get(G2::VARTOPCOUNT);
    assert(is_scalar($mixedBlockCount));
    $links = $this->top->getLinks((int) $mixedBlockCount);

    $result = [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
    return $result;
  }

}
