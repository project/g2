<?php

namespace Drupal\g2\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\g2\G2;

/**
 * Provides a g2 footnotes block.
 *
 * @Block(
 *   id = "g2_footnotes",
 *   admin_label = @Translation("G2 Footnotes"),
 *   category = @Translation("G2")
 * )
 */
class FootnotesBlock extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string,array<string,mixed>>
   */
  public function build() {
    $build['content'] = [
      '#markup' => '<div class=g2-footnotes></div>',
      '#attached' => [
        'library' => [G2::LIB_FOOTNOTES],
      ],
    ];
    $build['content']['#cache'] = [
      'max-age' => 0,
    ];
    return $build;
  }

}
